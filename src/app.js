const clock = document.createElement("div");
document.body.appendChild(clock);
clock.style.width = "100%";
clock.style.textAlign = "center";
clock.style.fontSize = "50pt";

function updateClock() {
  const now = new Date();
  const hours = now.getHours();
  const minutes = now.getMinutes();
  const seconds = now.getSeconds();

  // Format the string with leading zeroes
  const clockStr = `${hours.toString().padStart(2, "0")}:${minutes
    .toString()
    .padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`;

  clock.innerText = clockStr;
}

// start the clock
updateClock();
setInterval(updateClock, 1000);

// setup buttons
const btnMainThread = document.createElement("button");
btnMainThread.innerText = "Sleep in main thread";
document.body.appendChild(btnMainThread);

let span = document.createElement("span");
span.innerText = " or ";
document.body.appendChild(span);

const btnWebWorker = document.createElement("button");
btnWebWorker.innerText = "Sleep in Web Worker";
document.body.appendChild(btnWebWorker);

span = document.createElement("span");
span.innerText = " for ";
document.body.appendChild(span);

const inputTime = document.createElement("input");
inputTime.type = "number";
inputTime.min = 1;
inputTime.max = 100;
inputTime.value = 3;
inputTime.style.width = "50px";
document.body.appendChild(inputTime);

span = document.createElement("span");
span.innerText = " seconds";
document.body.appendChild(span);

const container = document.createElement("div");
container.style.width = "100%";
container.style.textAlign = "center";
const link = document.createElement("a");
link.innerText = "Advanced version";
link.href = "./advanced";
container.appendChild(link);
document.body.appendChild(container);

// start service worker + web worker + bind events
(async () => {
  if (!("serviceWorker" in navigator)) return;
  // just in case of a forced reload (controller is disabled in this case)
  // see https://github.com/w3c/ServiceWorker/blob/main/explainer.md#so-im-controlling-pages-now
  if (navigator.serviceWorker.controller == null) window.location.reload();

  // setup service worker
  try {
    await navigator.serviceWorker.register("./sw.js", {
      scope: "./",
    });
  } catch (error) {
    console.error(`Registration failed with ${error}`);
    return;
  }

  // this seems useless
  // await navigator.serviceWorker.ready;

  btnMainThread.addEventListener("click", () => {
    btnMainThread.disabled = true;
    const t0 = Date.now();
    const request = new XMLHttpRequest();
    request.open("GET", `./sleep?s=${inputTime.value}`, false); // synchronous request
    request.send(null);
    const t1 = Date.now();
    console.log(`Main thread slept for ${(t1 - t0) / 1000} s.`);
    btnMainThread.disabled = false;
  });

  if (!("Worker" in window)) return;
  // setup web worker
  const worker = new Worker("./worker.js");
  worker.onmessage = (e) => {
    const { action } = e.data;
    if (action === "wakeup") btnWebWorker.disabled = false;
  };
  btnWebWorker.addEventListener("click", () => {
    btnWebWorker.disabled = true;
    worker.postMessage({ action: "sleep", payload: inputTime.value });
  });
})();
