// service worker immediately available
// see https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerGlobalScope/skipWaiting
self.addEventListener("install", (event) => {
  event.waitUntil(self.skipWaiting());
});

// service worker immediately available
// see https://developer.mozilla.org/en-US/docs/Web/API/Clients/claim
self.addEventListener("activate", (event) => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener("fetch", (event) => {
  const request = event.request;
  const url = new URL(request.url);

  if (url.pathname.endsWith("sleep")) {
    const s = url.searchParams.get("s") ?? "1";
    const duration = 1000 * parseFloat(s);
    event.respondWith(
      (async () => {
        await new Promise((resolve) => setTimeout(resolve, duration));
        return new Response();
      })()
    );
  }
});
