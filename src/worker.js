onmessage = (e) => {
  const { action, payload } = e.data;
  if (action === "sleep") {
    const t0 = Date.now();
    const request = new XMLHttpRequest();
    request.open("GET", `./sleep?s=${payload}`, false); // synchronous request
    request.send(null);
    const t1 = Date.now();
    console.log(`Web Worker slept for ${(t1 - t0) / 1000} s.`);
    self.postMessage({ action: "wakeup" });
  }
};
