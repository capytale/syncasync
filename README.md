# Syncasync

Ce dépôt est un projet-jouet pour illustrer un concept assez fantastique en Javascrit :
il s'agit d'**exécuter** de manière **synchrone** un code **asynchrone**.
Une démo [se trouve ici](https://capytale.forge.apps.education.fr/syncasync).

## Principe

Pour cela, une [requête XHR synchrone](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest_API/Synchronous_and_Asynchronous_Requests#synchronous_request)
est envoyé à un [Service Worker](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers)
qui intercepte la requête et lance le code asynchrone.

Le résultat est l'exécution synchrone du code asynchrone puisque la requête XHR aboutit
lorsque la requête asynchrone est terminée et que le Service Worker a rendu la main.

## Thread principal / thread séparé

Si le mécanisme est exécuté dans le thread principal, il sera bloquant, c'est pourquoi, une seconde
implémentation utilisant un [Web Worker](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers)
est proposée.

## Implémentation

Pour cerner les mécanismes profonds, il a été choisi d'implémenter ceci en Javascript pur (vanilla).

## Version avancée

Dans la [version avancée](https://capytale.forge.apps.education.fr/syncasync/advanced)),
il est possible d'écrire son propre code Javascript (asynchrone)
pour l'exécuter de manière synchrone. Cette version utilise [Comlink](https://github.com/GoogleChromeLabs/comlink)
et des requêtes POST vers le Service Worker.
